import java.util.*;

public class Sorting{
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    ISort isort = new ISort();
    GSort gsort = new GSort();
    int maxTry = 1000;
    int times = 100;
    double[] data = new double[times];
    double[] dataM = new double[times];
    while(sc.hasNext()){
      int size = sc.nextInt();
      int[] a = new int[size];
      for(int i = 0; i < size; i++)
        a[i] = sc.nextInt();
      Statistics stat = null;
      Statistics statM = null;
      double min = Double.MAX_VALUE;
      double minM = Double.MAX_VALUE;
      double bestEstimated = 0;
      double bestEstimatedM = 0;
      do{
        int[] b = new int[size];
        System.arraycopy(a, 0, b, 0, a.length);
        int[] c = new int[size];
        System.arraycopy(a, 0, c, 0, a.length);
        long estimatedTime = 0;
        long estimatedTimeM = 0;
        for(int i = 0; i < times; i++){
          long startTime = System.nanoTime();
          isort.sort(b);
          estimatedTime = System.nanoTime() - startTime;
          data[i] = estimatedTime;
          startTime = System.nanoTime();
          gsort.sort(c,0,c.length-1);
          estimatedTimeM = System.nanoTime() - startTime;
          dataM[i] = estimatedTimeM;
        }
        stat = new Statistics(data);
        statM = new Statistics(dataM);
        maxTry--;
        if(stat.getVariance() < min){
          min = stat.getVariance();
          bestEstimated = estimatedTime;
        }
        if(statM.getVariance() < minM){
          minM = statM.getVariance();
          bestEstimatedM = estimatedTimeM;
        }
      }while(maxTry > 0 && stat.getVariance() > 5);
      System.out.println(size + " " + stat.getMean() + " " + statM.getMean());
    }
  }
}
