import java.util.*;
import java.io.*;
public class ArrayGenerator{
  private static int[] generate(int size){
    int[] array = new int[size];
    for(int i = size; i > 0; i--)
      array[size-i] = i;
    return array;
  }
  public static void print(PrintStream out, int[] a){
    int i;
    for(i = 0; i < a.length-1; i++)
      out.print(a[i] + " ");
    out.println(a[i]);
  }
  public static void main(String[] args){
    if(args.length < 3){
      System.err.println("Usage: $java ArrayGenerator <Min Size> <Max Size> <Interval>");
      System.exit(1);
    }

    int min = Integer.parseInt(args[0]);
    int max = Integer.parseInt(args[1]);
    int delta = Integer.parseInt(args[2]);

    for(int i = min; i <= max; i+=delta){
      System.out.println(i);
      print(System.out, generate(i));
    }
  }
}
