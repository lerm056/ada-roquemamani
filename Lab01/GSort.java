public class GSort{
  public int[] sort(int[] a,int p,int r){
    if (p < r){
      int q = (p + r) / 2;
      sort(a,p,q);
      sort(a,q+1,r);
      merge(a,p,q,r);
    }
    return a;
  }

  public void merge(int[] a, int p, int q, int r){
    int n1 = q - p + 1;
    int n2 = r - q;
    int i, j;
    int[] L = new int[n1 + 1], R = new int[n2 + 1];
    for(i = 0; i < n1; i++)
      L[i] = a[p+i];
    for(j = 0; j < n2; j++)
      R[j] = a[q+j+1];
    L[n1] = Integer.MAX_VALUE;
    R[n2] = Integer.MAX_VALUE;
    i = j = 0;
    for(int k = p; k <= r; k++){
      if(L[i] < R[j]){
        a[k] = L[i];
        i++;
      } else {
        a[k] = R[j];
        j++;
      }
    }
  }
}
