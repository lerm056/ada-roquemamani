import java.util.*;
public class Search{
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    BinarySearch bin = new BinarySearch();
    int times = sc.nextInt();
    int sum = 0;
    while(sc.hasNext()){
      int size = sc.nextInt();
      int[] a = new int[size];
      long start, end;
      for(int i = 0; i < times; i++){
        for(int j = 0; j < size; j++)
          a[i] = sc.nextInt();
        start = System.nanoTime();
        bin.binary(a,33);
        end = System.nanoTime() - start;
        sum += end;
      }
    }
  }
}
