public class BinarySearch{
  public static int binary(int[] a, int b){
    int left = 0;
    int rigth = a.length - 1;
    int p;
    do{
      p = (left + rigth) / 2;
      if(a[p] == b){
        return p;
      } else {
        if(b > a[p]){
          left = p + 1;
        } else {
          rigth = p - 1;
        }
      }
    } while(left <= rigth);
    return -1;
  }
}
