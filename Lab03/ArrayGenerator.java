import java.util.*;
public class ArrayGenerator{
  private static int[] generate(int size){
    int[] array = new int[size];
    int i;
    for(i = 0; i < size - 1; i++)
      array[i] = 1 + (int)(Math.random()*110);
    array[i] = 33;
    return array;
  }
  public static void print(int[] a){
    int i;
    for(i = 0; i < a.length - 1; i++)
      System.out.print(a[i] + " ");
    System.out.println(a[i]);
  }
  public static int[] sort(int[] a){
    for(int j = 1; j < a.length; j++){
      int key = a[j];
      int i = j - 1;
      while(i >= 0 && a[i] > key){
        a[i+1] = a[i];
        i = i - 1;
      }
      a[i+1] = key;
    }
    return a;
  }
  public static void main(String[] args){
    if(args.length < 3){
      System.err.println("Usage: $java ArrayGenerator <Min size> <Max size> <Times>");
      System.exit(1);
    }

    int min = Integer.parseInt(args[0]);
    int max = Integer.parseInt(args[1]);
    int times = Integer.parseInt(args[2]);

    System.out.println(times);
    for(int i = min; i <= max; i++){
      System.out.println(i);
      for(int j = 0; j < times; j++)
        print(sort(generate(i)));
    }
  }
}
