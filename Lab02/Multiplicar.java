public class Multiplicar{
  public static void main(String[] args){
    if(args.length != 3){
      System.err.println("Usage: $java Multiplicar <first size> <last size> <algorithm>");
      System.exit(1);
    }
    int first = Integer.parseInt(args[0]);
    int last = Integer.parseInt(args[1]);
    int algo = Integer.parseInt(args[2]);
    if(algo == 0){
      normal(first, last);
    }else if(algo == 1){
      strassen(first, last);
    }else{
        System.err.println("Usage: $java Multiplicar <first size> <last size> <times per size> <algorithm>");
        System.exit(1);
    }
  }
  private static void normal(int first, int last){
    int times = 100;
    int maxTry = 100;
    int[][] a, b, c;
    double[] data = new double[times];
    Statistics stat;
    MulMat mul = new MulMat();
    for(int i = first; i <= last; i*=2){
      a = new int[i][i];
      b = new int[i][i];
      c = new int[i][i];
      double min = Double.MAX_VALUE;
      double bestEstimated = 0;
      do{
        a = MatrixGenerator.generate(i);
        b = MatrixGenerator.generate(i);
        long estimatedTime = 0;
        for(int j = 0; j < times; j++){
          long startTime = System.nanoTime();
          c = mul.mulMat(a,b);
          estimatedTime = System.nanoTime() - startTime;
          data[i] = estimatedTime;
        }
        stat = new Statistics(data);
        maxTry--;
        if(stat.getVariance() < min){
          min = stat.getVariance();
          bestEstimated = estimatedTime;
        }
      } while (stat.getVariance() > 3 && maxTry > 0);
      System.out.println(i + " " + stat.getMean());
    }
  }
  private static void strassen(int first, int last){
    int times = 100;
    int maxTry = 100;
    int[][] a, b, c;
    double[] data = new double[times];
    Statistics stat;
    StraMat mul = new StraMat();
    for(int i = first; i <= last; i*=2){
      a = new int[i][i];
      b = new int[i][i];
      c = new int[i][i];
      double min = Double.MAX_VALUE;
      double bestEstimated = 0;
      do{
        a = MatrixGenerator.generate(i);
        b = MatrixGenerator.generate(i);
        long estimatedTime = 0;
        for(int j = 0; j < times; j++){
          long startTime = System.nanoTime();
          c = mul.mulMat(a,b);
          estimatedTime = System.nanoTime() - startTime;
          data[i] = estimatedTime;
        }
        stat = new Statistics(data);
        maxTry--;
        if(stat.getVariance() < min){
          min = stat.getVariance();
          bestEstimated = estimatedTime;
        }
      } while (stat.getVariance() > 3 && maxTry > 0);
      System.out.println(i + " " + stat.getMean());
    }
  }
}
