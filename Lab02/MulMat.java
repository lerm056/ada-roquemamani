public class MulMat{
  
  private static void gen(int[][] a){
    for(int i = 0; i < a.length; i++)
      for(int j = 0; j < a[0].length; j++)
        a[i][j] = 0;
  }

  private int[][] divMat(int[][] a, int x, int y){
    int size = a.length;
    int[][] b = new int[size/2][size/2];
    if(x == 1){
      if(y == 1){
        for(int i = 0; i < size/2; i++)
          for(int j = 0; j < size/2; j++)
            b[i][j] = a[i][j];
      } else {
       for(int i = 0; i < size/2; i++)
        for(int j = size/2; j < size; j++)
         b[i][j-(size/2)] = a[i][j];
      }
    } else {
      if(y == 1){ 
        for(int i = size/2; i < size; i++)
          for(int j = 0; j < size/2; j++)
            b[i-(size/2)][j] = a[i][j];
      } else {
       for(int i = size/2; i < size; i++)
        for(int j = size/2; j < size; j++)
         b[i-(size/2)][j-(size/2)] = a[i][j];
      }
    }
    return b;
  }

  private int[][] sumMat(int[][] a, int[][] b){
    int size = a.length;
    int[][] c = new int[size][size];
    for(int i = 0; i < size; i++)
      for(int j = 0; j < size; j++)
        c[i][j] = a[i][j] + b[i][j];
    return c;
  }

  private int[][] uniMat(int[][] a11, int[][] a12, int[][] a21, int[][] a22){
    int size = a11.length;
    int[][] a = new int[size*2][size*2];
    for(int i = 0; i < size; i++){
      for(int j = 0; j < size; j++){
        a[i][j] = a11[i][j];
        a[i][j+size] = a12[i][j];
        a[i+size][j] = a21[i][j];
        a[i+size][j+size] = a22[i][j];
      }
    }
    return a;
  }

  public int[][] mulMat(int[][] a, int[][] b){
    int size = a.length;
    int[][] c = new int[size][size];
    if(size == 1){
      c[0][0] = a[0][0] * b[0][0];
    } else {
      int[][] a11 = divMat(a, 1, 1);
      int[][] a12 = divMat(a, 1, 2);
      int[][] a21 = divMat(a, 2, 1);
      int[][] a22 = divMat(a, 2, 2);
      int[][] b11 = divMat(b, 1, 1);
      int[][] b12 = divMat(b, 1, 2);
      int[][] b21 = divMat(b, 2, 1);
      int[][] b22 = divMat(b, 2, 2);
      int[][] c11 = divMat(c, 1, 1);
      int[][] c12 = divMat(c, 1, 2);
      int[][] c21 = divMat(c, 2, 1);
      int[][] c22 = divMat(c, 2, 2);
      c11 = sumMat(mulMat(a11, b11), mulMat(a12, b21));
      c12 = sumMat(mulMat(a11, b12), mulMat(a12, b22));
      c21 = sumMat(mulMat(a21, b11), mulMat(a22, b21));
      c22 = sumMat(mulMat(a21, b12), mulMat(a22, b22));
      c=uniMat(c11, c12, c21, c22);
    }
    return c;
  }
}
