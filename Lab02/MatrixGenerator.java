import java.util.*;
import java.io.*;
public class MatrixGenerator{
  public static int[][] generate(int size){
    int[][] matrix = new int[size][size];
    for(int i = 0; i < size; i++)
      for(int j = 0; j < size; j++)
        matrix[i][j] = (int)(Math.random()*6);
    return matrix;
  }

  public static void print(PrintStream out, int[][] a){
    int j;
    for(int i = 0; i < a.length; i++){
      for(j = 0; j < a[0].length - 1; j++)
        out.print(a[i][j] + " ");
      out.println(a[i][j]);
    }
  }
  
  public static void main(String[] args){
    if(args.length == 0){
      System.err.print("Usage: $java MatrixGenerator <array size>");
    }
    int size = Integer.parseInt(args[0]);
    System.out.println(size);
    print(System.out, generate(size));
  }
}
