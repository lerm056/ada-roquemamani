public class StraMat{
  
  private static void gen(int[][] a){
    for(int i = 0; i < a.length; i++)
      for(int j = 0; j < a[0].length; j++)
        a[i][j] = 0;
  }

  private int[][] divMat(int[][] a, int x, int y){
    int size = a.length;
    int[][] b = new int[size/2][size/2];
    if(x == 1){
      if(y == 1){
        for(int i = 0; i < size/2; i++)
          for(int j = 0; j < size/2; j++)
            b[i][j] = a[i][j];
      } else {
       for(int i = 0; i < size/2; i++)
        for(int j = size/2; j < size; j++)
         b[i][j-(size/2)] = a[i][j];
      }
    } else {
      if(y == 1){ 
        for(int i = size/2; i < size; i++)
          for(int j = 0; j < size/2; j++)
            b[i-(size/2)][j] = a[i][j];
      } else {
       for(int i = size/2; i < size; i++)
        for(int j = size/2; j < size; j++)
         b[i-(size/2)][j-(size/2)] = a[i][j];
      }
    }
    return b;
  }

  private int[][] sumMat(int[][] a, int[][] b){
    int size = a.length;
    int[][] c = new int[size][size];
    for(int i = 0; i < size; i++)
      for(int j = 0; j < size; j++)
        c[i][j] = a[i][j] + b[i][j];
    return c;
  }

  private int[][] resMat(int[][] a, int[][] b){
    int size = a.length;
    int[][] c = new int[size][size];
    for(int i = 0; i < size; i++)
      for(int j = 0; j < size; j++)
        c[i][j] = a[i][j] - b[i][j];
    return c;
  }

  private int[][] uniMat(int[][] a11, int[][] a12, int[][] a21, int[][] a22){
    int size = a11.length;
    int[][] a = new int[size*2][size*2];
    for(int i = 0; i < size; i++){
      for(int j = 0; j < size; j++){
        a[i][j] = a11[i][j];
        a[i][j+size] = a12[i][j];
        a[i+size][j] = a21[i][j];
        a[i+size][j+size] = a22[i][j];
      }
    }
    return a;
  }

  public int[][] mulMat(int[][] a, int[][] b){
    int size = a.length;
    int[][] c = new int[size][size];
    if(size == 1){
      c[0][0] = a[0][0] * b[0][0];
    } else {
      int[][] a11 = divMat(a, 1, 1);
      int[][] a12 = divMat(a, 1, 2);
      int[][] a21 = divMat(a, 2, 1);
      int[][] a22 = divMat(a, 2, 2);
      int[][] b11 = divMat(b, 1, 1);
      int[][] b12 = divMat(b, 1, 2);
      int[][] b21 = divMat(b, 2, 1);
      int[][] b22 = divMat(b, 2, 2);
      int[][] c11 = divMat(c, 1, 1);
      int[][] c12 = divMat(c, 1, 2);
      int[][] c21 = divMat(c, 2, 1);
      int[][] c22 = divMat(c, 2, 2);
      int[][] s1 = resMat(b12, b22);
      int[][] s2 = sumMat(a11, a12);
      int[][] s3 = sumMat(a21, a22);
      int[][] s4 = resMat(b21, b11);
      int[][] s5 = sumMat(a11, a22);
      int[][] s6 = sumMat(b11, b22);
      int[][] s7 = resMat(a12, a22);
      int[][] s8 = sumMat(b21, b22);
      int[][] s9 = resMat(a11, a21);
      int[][] s10 = sumMat(b11, b12);
      int[][] p1 = mulMat(a11, s1);
      int[][] p2 = mulMat(s2, b22);
      int[][] p3 = mulMat(s3, b11);
      int[][] p4 = mulMat(a22, s4);
      int[][] p5 = mulMat(s5, s6);
      int[][] p6 = mulMat(s7, s8);
      int[][] p7 = mulMat(s9, s10);
      c11 = sumMat(resMat(sumMat(p5, p4), p2), p6);
      c12 = sumMat(p1, p2);
      c21 = sumMat(p3, p4);
      c22 = resMat(resMat(sumMat(p5, p1), p3), p7);
      c=uniMat(c11, c12, c21, c22);
    }
    return c;
  }
}
